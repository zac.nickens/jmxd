#  JMXd Primerica Alpha

## This tool is still in alpha and is currently under continued development by Primerica SRE Tiger Team. 

**JMXd currently works to scrape itself for proof of concept. Development and implementation is in progress for Mule-EE based containers. Implementation for Springboot is in progress**

JMXd is based on containerization effort orgininally done by eedugon

JMXd is a dockerised JMX Exporter image, based on the original project created by sscaling (sscaling/docker-jmx-prometheus-exporter), using alpine-java, dumb-init and fetching the official released version of jmx_exporter from the [maven central repository](https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_httpserver/)

This version of JMXd includes the following extra features:

  * Automatic Configuration: config.xml creation based on environment variables and templates.
  * Static Configuration: if config.xml is mapped into /opt/jmx_exporter/config/, then environment variables won't be considered, and the provided configuration will be used.
  * Flexible rules: via environment variables we can select the rules to apply to the configuration (only if Automatic Configuration feature is used and only 1 rules file allowed).
  * check_init : With check init enabled (by default is disabled), jmx_exporter will wait until the remote jmx port is available (this feature uses nagios check_jmx plugin) before starting the exporter itself.
  * k8s deploy manifest templates for standalone and sidecar patterns.

Environment variables supported to get different behavior/configuration:

***a helm chart is in development for deploying***
  * SERVICE_PORT -- Port to receive http /metrics requests from Prometheus.
  * DEST_HOST -- Host to monitor via jmx.
  * DEST_PORT -- JMX port of destination host.
  * RULES_MODULE -- Rules to apply.
  * JVM_LOCAL_OPTS -- Options for local jvm.
  * JMX_LOCAL_PORT -- Port for local jmxremote.
  * CHECK_INIT -- (true | false) - Enable/disable check_init feature.
  * CHECK_INIT_ACTION -- (exit | continue) -- What to do in case of failing checks.
  * CHECK_INIT_MAX_DELAY --  Maximum time to spend checking remote JVM.
  * CHECK_INIT_INTERVAL -- Interval between attempts (in seconds).

**Kafka basic support is only alpha. No POC has been developed at this time**
Supported modules in current version (only one can be selected):
  * default (this will translate all mbeans to metrics)
  * kafka-0-2-8

The objectives of implementing these features are:

  * Being able to use the same docker image of jmx_exporter for different scenarios, as the configuration can be built from environment variables.
  * In a Kubernetes environment, run jmxd as an extra container (sidecar) in the pod to monitor (in this case DEST_HOST should be localhost).
  * In a Kubernetes environment, run jmxd in a dedicated pod, in order to monitor another pod (in this case DEST_HOST should be the name of a k8s service pointing to the pod).

## Default Settings

If no environment variables or volumes are provided to the image, the exporter will have the following default behavior:

  * HTTP listening port: 9072
  * Remote JVM to connect to: localhost: 7072
	* Rules to apply: default (which means a simple pattern: ".\*" )
	* Local jmxremote port: 7071 (in case someone wants to check this JVM)
  * CHECK_INIT module will be disabled by default.

## Building docker image

	docker build -t jmxd .

## Running local

	docker run --rm -p "9072:9072" jmxd

Then you can visit the metrics endpoint: [http://127.0.0.1:9072/metrics]

Note: If you want to test jmx_exporter monitoring it's own metrics, you could set DEST_PORT to 7071, which is the jmx port of the jmx_exporter itself.

  docker run --rm -e "DEST_PORT=7071" -p "9072:9072" jmxd


## Configuration modes: Auto generation of config.xml vs static configuration

This docker allows 2 configuration modes, automatic generation of config.xml (default) or providing a static configuration file via volume mount.

Example of setting our own config:

	docker run --rm -p "9072:9072" -v "$PWD/config.yml:/opt/jmx_exporter/config/config.yml" jmxd

The configuration options are documented: [https://github.com/prometheus/jmx_exporter](https://github.com/prometheus/jmx_exporter)

### Environment variables

Additionally, the following environment variables can be defined

* SERVICE_PORT -- port to receive http /metrics requests
* DEST_HOST -- host to monitor via jmx
* DEST_PORT -- jmx port of destination host
* RULES_MODULE -- rules to apply
* JVM_LOCAL_OPTS -- options for local jvm
* JMX_LOCAL_PORT -- port for local jmxremote

### k8s manifest
Sample k8s deployment manifests are included
`jmxd-deploy.yaml` Use this as a template for deploying as a sidecar. 
`jmxd-standalone.yaml` k8s manifest for deploying jmxd as a standalone workload. 
`jmxd-service.yaml` provided for creating a ClusterIP service to access app to be monitored in standalone mode.



